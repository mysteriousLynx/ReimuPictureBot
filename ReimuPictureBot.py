#!/bin/env/python2
# coding: utf-8
import simplejson as json
import praw
import requests
import random
import time
from imgurpython import *
import configparser

def returnPosts(prms):
    baseURL = "https://danbooru.donmai.us"
    r = requests.get(baseURL + "/posts.json", params=prms)
    return r.text

def file_len(fname):
    with open(fname, "r+") as f:
        for i, l in enumerate(f):
            pass
    try:
        return i + 1
    except UnboundLocalError:
        return 0

def main():
    allowedFileFormats = ["jpeg", "jpg", "png", "gif", "apng", "tiff", "pdf", "xcf"]
    subredditToPostOn = 'Reimu'
    warning = \
"""Here's the source: {}

---

*I am a bot, and this action was performed automatically. Please contact /u/xxtgzxx if you have any questions or concerns.*"""

    config = configparser.RawConfigParser()
    config.read('config')

    client = ImgurClient(client_id=config.get('Imgur Client', 'client_id'),
                         client_secret=config.get('Imgur Client', 'client_secret'))

    reddit = praw.Reddit(client_id=config.get('Reddit Client', 'client_id'),
                         client_secret=config.get('Reddit Client', 'client_secret'),
                         user_agent=config.get('Reddit Client', 'user_agent'),
                         username=config.get('Reddit Client', 'username'),
                         password=config.get('Reddit Client', 'password'))

    # requests items and only adds to list if meet requirements
    jsonPosts = []
    with open("idList.txt", "r+") as test:
        ids = []
        for id in test.readlines():
            try:
                ids.append(int(id))
            except ValueError:
                pass
        for idx in range(0, 1000):
            jsonReturned = json.loads(returnPosts({'limit': 9999, 'tags': 'hakurei_reimu favcount:>75', 'page': idx}))
            if str(jsonReturned) == "[]":
                break
            else:
                for idx in jsonReturned:
                    try:
                        if idx['score'] >= 50 and idx['file_ext'].lower() in allowedFileFormats:
                                if idx['id'] not in ids:
                                    jsonPosts.append(idx)
                    except KeyError:
                        pass

    # chooses random item and post on reddit
    choice = random.choice(jsonPosts)
    danbooruPage = "https://danbooru.donmai.us/posts/" + str(choice['id'])
    title = "Daily Reimu Picture " + time.strftime("%m/%d/%Y")
    image = client.upload_from_url(url="https://danbooru.donmai.us" + choice['file_url'])
    post = reddit.subreddit(subredditToPostOn).submit(title=title, url=image['link'])

    # update id list
    with open("idList.txt", "a+") as f:
        amntOfLines = file_len("idList.txt")
        if amntOfLines == 0:
            f.write(str(choice['id']))
        else:
            f.write("\n" + str(choice['id']))

    if str(choice['rating']) != "s":
        post.mod.nsfw()

    # make reply
    if choice['pixiv_id']:
        reply = warning.format(
            "https://www.pixiv.net/member_illust.php?mode=medium&illust_id=" + str(choice['pixiv_id']))
        post.reply(reply)
    else:
        if choice['source'] is not "":
            reply = warning.format(str(choice['source']))
            post.reply(reply)
        else:
            reply = warning.format(danbooruPage)
            post.reply(reply)

    # update logs
    with open("logs.txt", "a+") as f:
        amntOfLines = file_len("logs.txt")
        if amntOfLines == 0:
            f.write("[Submission]")
        else:
            f.write("\n\n[Submission]")

        if time.strftime("%d").endswith("1"):
            ordinalIndicator = "st"
        elif time.strftime("%d").endswith("2"):
            ordinalIndicator = "nd"
        elif time.strftime("%d").endswith("3"):
            ordinalIndicator = "rd"
        else:
            ordinalIndicator = "th"
        f.write(time.strftime("\n%A, %B ") + time.strftime("%d").lstrip("0") + ordinalIndicator + time.strftime(
            ", %Y at %H:%M"))

        f.write("\nSubreddit: " + subredditToPostOn)
        f.write("\nTitle: " + title)
        f.write("\nSubmission Link: " + post.shortlink)
        f.write("\nURL: " + image['link'])
        f.write("\nDanbooru page: " + danbooruPage)
        f.write("\nComment: \"" + reply + "\"")

if __name__ == "__main__":
    while True:
        if str(time.strftime("%H:%M")) == "00:00":
            main()
            while str(time.strftime("%H:%M")) == "00:00":
                time.sleep(1)
        time.sleep(1)